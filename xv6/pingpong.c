#include "types.h"
#include "stat.h"
#include "user.h"
#include "semaphore.h"

int
main(int argc, char *argv[])
{
  if (argc < 2 ) {
    printf(1, "Argument N was not received");
  } else {
    int nping, npong;
    nping = npong = atoi(argv[1]);
    int sping = 1;
    int spong = 2;
    sem_open(sping, SEM_OPEN_OR_CREATE, 1);
    sem_open(spong, SEM_OPEN_OR_CREATE, 0);
    if (fork()) { // parent: ping
      while (nping) {
        sem_down(sping);
        printf(1, "ping\n");
        nping--;
        sem_up(spong);
      }
      wait();
      sem_close(sping);
    } else { // child: pong
      while (npong) {
        sem_down(spong);
        printf(1, "\tpong\n");
        npong--;
        sem_up(sping);
      }
      sem_close(spong);
    }
  }
  exit();
}
