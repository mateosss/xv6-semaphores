#include "spinlock.h"

#define SEM_CREATE 0
#define SEM_OPEN 1
#define SEM_OPEN_OR_CREATE 2

struct semaphore {
    int value;
    int pcount; // Amount of processes with this semaphore opened
    int created; // Is the semaphore alive
};
