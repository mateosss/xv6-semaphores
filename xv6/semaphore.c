// Semaphores garanted to be atomic in sem_up and sem_up only
// The creation, opening, closing and distruction should be done secuentialy
// Be aware of possible race conditions in these cases

#include "types.h"
#include "defs.h"
#include "param.h"
#include "semaphore.h"

struct {
  struct spinlock lock;
  struct semaphore semaphores[NSEMAPHORES];
} stable;

void
sinit(void)
{
  initlock(&stable.lock, "stable");
}

int
s_exists(int semaphore)
{
  return semaphore < NSEMAPHORES && stable.semaphores[semaphore].created;
}

// Pre: !s_exists(semaphore) && semaphore < NSEMAPHORES
void
s_create(int semaphore, int value)
{
  stable.semaphores[semaphore].created = 1;
  stable.semaphores[semaphore].value = value;
  stable.semaphores[semaphore].pcount = 0;
}

// Pre: semaphore < NSEMAPHORES
void
s_destroy(int semaphore)
{
  stable.semaphores[semaphore].created = 0;
}

// Pre: s_exists(semaphore)
void
s_open(int semaphore)
{
  stable.semaphores[semaphore].pcount++;
}

// Pre: s_exists(semaphore)
int
s_close(int semaphore)
{
  // This condiotional could be optional based on your views on semaphores
  // If you think they are like files, then after everyone finish using them
  // they remain created
  // If you think they are like processes, then after it finish its job it
  // should be destroyed, we choose this one.
  if (stable.semaphores[semaphore].pcount == 1) {
    s_destroy(semaphore);
  }
  return stable.semaphores[semaphore].pcount--;
}

// Pre: s_exists(semaphore)
void
s_up(int semaphore)
{
  struct semaphore *s = &stable.semaphores[semaphore];

  acquire(&stable.lock);
  s->value++;
  if (s->value == 1) {
    wakeup(s);
  }
  release(&stable.lock);
}

// Pre: s_exists(semaphore)
void
s_down(int semaphore)
{
  struct semaphore *s = &stable.semaphores[semaphore];

  acquire(&stable.lock);
  while (s->value == 0) {
    sleep(s, &stable.lock);
  }
  s->value--;
  release(&stable.lock);
}
