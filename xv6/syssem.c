//
// Semaphore system calls.
// Similar to sysfile.c but for semaphores
//

#include "types.h"
#include "defs.h"
#include "param.h"
#include "semaphore.h"

int
sys_sem_open(void)
{
  int semaphore, flags, value;

  if(argint(0, &semaphore) < 0 || argint(1, &flags) < 0 || argint(2, &value) < 0)
    return -1;

  switch (flags) {
    case SEM_CREATE:
      if (!s_exists(semaphore)) {
        s_create(semaphore, value);
        return 0;
      } else {
        return -1;
      }
      break;
    case SEM_OPEN:
      if (s_exists(semaphore)) {
        s_open(semaphore);
        return 0;
      } else {
        return -1;
      }
      break;
    case SEM_OPEN_OR_CREATE:
      if (!s_exists(semaphore)) {
        s_create(semaphore, value);
      } else if (semaphore >= NSEMAPHORES) {
        return -1;
      } else {
        s_open(semaphore);
      }
      return 0;
      break;
  }
  return 0;
}

int
sys_sem_close(void)
{
  int semaphore;

  if(argint(0, &semaphore) < 0)
    return -1;

  if(!s_exists(semaphore)) {
    return -1;
  } else {
    return s_close(semaphore);
  }
}

int
sys_sem_up(void)
{
  int semaphore;

  if(argint(0, &semaphore) < 0)
    return -1;

  if(!s_exists(semaphore)) {
    return -1;
  } else {
    s_up(semaphore);
    return 0;
  }
}

int
sys_sem_down(void)
{
  int semaphore;

  if(argint(0, &semaphore) < 0)
  return -1;

  if(!s_exists(semaphore)) {
    return -1;
  } else {
    s_down(semaphore);
    return 0;
  }
}
