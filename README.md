# Lab 2: Semáforos

## Introducción

Pensando en semáforos como recursos del sistema intentamos copiar de otros lados
donde haya recursos similares (en particular recursos con locks). Uno de estos
recursos son los procesos, donde hay una lista de ellos y cada proceso actúa
como un semáforo en ese sentido. Por lo cual, hicimos semaphore.c/h basándonos
en proc.c/h (ver stable, ptable) y, a su vez, como en sysfile.c están las
syscalls relacionadas al file system y en en sysproc.c las relacionadas a los
procesos, entonces creemos syssem.c para hacer las syscalls relacionadas a los
semáforos con la misma idea. 

Para implementar los semáforos decidimos usar un único spinlock y, como todos los
semáforos están definidos en la `stable` (así como existe la ftable y la
ptable), no se puede usar mas de un semáforo a la vez. Esto lo hicimos ya que los
procesos en xv6 están diseñados de esta forma y quisimos respetar esta estructura.

Cada semáforo esta definido de la siguiente forma:

```c
struct semaphore {
    int value;
    int pcount;
    int created;
};
```

Donde:
- `value` representa el valor del semáforo.
- `pcount` representa la cantidad de procesos que estan usando ese semáforo.
- `created` representa el estado del creación de semáforo (1 si está creado y
  0 en caso contrario).

Además, cada función de semaphore.c tiene precondiciones que, quién las llame,
deberá asegurar que se cumplen.

A la hora de implementar ´pingpong´ tuvimos que hacer una llamada a ´wait()´
para asegurarnos que ping termine antes que pong y no se impriman mensajes de
´zombie!´.  

## Barriers

### ¿Por qué `&` al final de `barrier_echo`?

Cuando el shell corre un comando hace un fork en el cual el padre se queda
haciendo un `wait` y el hijo corre el comando en otro proceso. El `wait` hace
un `sleep` y se despierta cuando el comando (el hijo) termina y lo libera de la
process table. Esto hace que, cuando uno corre un comando normalmente, el shell
no vuelva a aprecer hasta que el comando hijo termine.

Al poner el `&` el comando se corre como uno de tipo `BACK`, el cual, además
del fork original que hace el shell, hace otro para finalmente ejecutar el
comando. Inmediatamente luego del fork este termina, lo que libera el shell
para poder seguir recibiendo input mientras el comando original es ejecutado
en paralelo.

Si no usáramos `&` al llegar al `sem_down` `barrier_echo` hace un sleep y, al
no hacer nunca el `exit()`, el control nunca vuelve al shell. El resultado
es que el sistema queda congelado e incapaz de poder recibir input.

### ¿Cómo puede simplificarse la implementación del barrier_release.c?

Si wakeup fuera una syscall podríamos usarla en lugar del ciclo pasándole el
channel del semáforo SEM_BARRIER, ahorrándonos de hacer un up por cada proceso.
Esto generaría el mismo resultado: que todos los procesos dormidos en el
semáforo terminen su ejecución.


## Porque aparece `zombie!`

`ZOMBIE` es un estado en el que quedan los procesos hijos luego de terminar
sus tareas. Estos no mueren sino que quedan en este estado esperando para
ser llamados por sus padres, ya que ellos son los encargados de liberar la
memoria asociada al proceso. Si se da el caso en que el proceso padre termine
antes que sus hijos, estos son adoptados por el proceso init y luego es
este el encargado de liberarlos una vez que terminen. En particular, cuando
esto pasa, init imprime "zombie!" por pantalla. El proceso de revisar y limpiar
que hijos son zombies ocurre cuando el proceso padre se despierta.


Es muy fácil generar un `zombie!` siempre que ejecutamos algún comando con `&`
al final. El arbol de ejecución sería de este estilo para un `barrier_echo hola
&`:

- `(pid:1)` Bootea xv6 y corre el `init` user program, el cual forkea:
  - `(pid:1)` Padre: se queda loopeando esperando por procesos abandonados que terminen
  su ejecución y no tengan padres para limpiar su process table.
  - `(pid:2)` Hijo: `sh`, el shell, y se queda en un bucle esperando comandos.
    Llega el comando `barrier_echo hola &` y esto hace que forkee de nuevo.
      - `(pid:2)` Padre: se queda haciendo wait esperando que el hijo termine para seguir
        con el shell.
      - `(pid:3)` Hijo: se va a correr el comando que es de tipo `BACK`, esto
        hace que forkee de vuelta.
          - `(pid:3)` Padre: se saltea el `switch` y va directo a `exit()` lo
            cual pone a `(pid:3)` en modo zombie y todos sus hijos son  migrados
            a ser hijos de `(pid:1)` el `initproc`. Además, ahora hace un awake a
            su padre `(pid:2)`, el cual lo limpia de la process table. A su vez,
            ahora `(pid:2)` puedo volver a recibir comandos del shell.
            _**`(pid:3)`** ha muerto y **`(pid:2)`** puede continuar recibiendo
            comandos._
          - `(pid:4)` Hijo: ejecuta el comando `barrier_echo hola` sin `&`, para
            cuando termine es probable que `(pid:3)` ya haya sido eliminado y
            por ende ahora el padre de este proceso sea `(pid:1)`.
            (`initproc`), una vez que termina el echo, se hace un exit, el cual
            hace un awake al padre `(pid:1)`. Este último estaba atento esperando a
            un hijo abandonado que hiciera un exit para gritar `zombie!`, aunque
            antes limpiando la process table del zombie. _**`(pid:4)`** ha
            muerto y **`(pid:1)`** ha printeado `zombie!` y puede volver a
            esperar a un nuevo zombie._

## Porque aparece `$`

El signo `'$'` es el símbolo utilizado por el `shell` para avisar que esta listo
para recibir un comando. Se puede dar el caso, durante la ejecución de algún
proceso en segundo plano (ej. `barrier_echo STRING_LARGO &`), que imprima por
pantalla y que el scheduler en el medio de la impresión decida darle control al
shell, el cual va a imprimir `$` en el medio de primera otra impresión.

Podemos ver como ocurre esto con un simple ejemplo usando los barriers:
- Primero inicializamos la barrera con `barrier_init` y luego ejecutamos un
  `barrier_echo` con muchos guiones para ver cuando aparece '$':

      $ barrier_echo _________________________________________________________ &

- Luego liberamos las barrier con `barrier_release` y vemos el resultado:

      _____$ ____________________________________________________ zombie!

Lo que esta pasando acá es que cuando llamamos a `barrier_release` comienza a
imprimirse el echo pero, luego de haber impreso algunos ' _ ', el scheduler deja
de ejecutar echo y vuelve a ejecutar el shell, por lo que se imprime `$` (que
básicamente es el shell esperando a algún comando). Pero, como este no tiene
mucho mas para hacer termina su quanto y el scheduler continua ejecutando el
barrier_echo hasta que termina.

El porque aparece un `zombie!` al final del print se debe a que `barrier_echo`
es un hijo de `barrier_echo &` y, como se explicó en la sección de `zombie!`,
esto se produce porque el proceso queda sin padre luego de que ejecutamos `&`.
Es decir, `barier_echo &` hace un `fork` y ejecuta en el hijo creado el `echo`
en segundo plano. Luego de hacer esto `barrier_echo &` ya no tiene nada que
hacer y se elimina pero su hijo queda sin padre entonces tiene que ser adoptado
por `init` quien luego se encarga de liberarlo.


## Explicación de funciones

### Argint

Cuando se produce una `syscall` xv6 genera un cambio de contexto al kernel, un
trap, guardando el contexto del proceso de usuario en un `trapframe`. Luego, el
sistema llama a la función `sys_syscall` que se encarga de recuperar los
argumentos originales del stack. Para esto hay que usar `argint(n, *v)`, que
toma el argumento `n` del stack pointer (`esp`) del trapframe del proceso actual
y lo guarda en la dirección `v`.

### Acquire

`acquire` recibe un `spinlock`, deshabilita las interrupciones y checkea
que ningún proceso corriendo en el mismo cpu tenga el lock que se está pidiendo.
Si ese fuera el caso habría deadlock ya que, el proceso que tiene el lock nunca
va a correr por que se deshabilitaron las interrupciones. Luego, se queda
haciendo busywait de `xchg` (una instrucción atómica que lee y pisa el lock).
Este busywait va a terminar solo si alguien liberó el lock. Si esto pasa,
sincronizamos las caches con `__sync_synchronize` y recolecta algunos datos
de debugging.

### Release

`release` deshace todos los efectos del `acquire`, en particular checkea que
antes se haya hecho un `acquire` (`!holding(lk)`), limpia la información de
debug, setea el lock en 0 (con una instrucción atómica en assembler) y por
último reactiva las interrupciones.

### Sleep

`sleep` toma un channel en el que dormirse (que suele ser la dirección de
memoria del recurso sobre el que se está durmiendo) y un lock (el del recurso).
Hace un `acquire` de la process table y suelta el lock original. Además, setea
el channel a chan y el estado del proceso a `SLEEPING`. Luego de eso se hace una
llamada al `scheduler()` y, mientras el proceso esté en `SLEEPING`, el scheduler
no lo va a elegir. Luego de que un `wakeup` le modifique el estado y el
scheduler lo elija, se limpia el `chan` del proceso, se libera la `ptable` y se
vuelve a adquirir el lock del recurso original.

### Wakeup

`wakeup` wrapea `wakeup1` en un acquire y release de la process table. A su vez,
`wakeup1` revisa toda la tabla de procesos y verifica cuales están durmiendo en
`chan` para despertarlos (setear su state a `RUNNABLE`).
